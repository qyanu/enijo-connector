# SPDX-License-Identifier: MIT
# Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
# Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
#
# This file forms part of the 'enijo-connector' project, see the
# project's readme, notes, and other documentation, including the
# license information, for further details.


BUILDDIR = build
DISTDIR = dist
EXTERNDIR = extern

.PHONY: default
default: help


.PHONY: help
help:
	@echo "Available targets:"
	@echo ""
	@echo "  help      display this help (the default)."
	@echo "  all       equivalent to executing all targets in order:"
	@echo "            'clean', 'dist', and 'docs'."
	@echo "  dist      produces files ready for destribution into directory"
	@echo "            'dist/', intermediary files (if any) are put into"
	@echo "            directory 'build/'."
	@echo "  publish-pypi  publish wheel and sdist from directory dist/"
	@echo "            to pypi."
	@echo "  publish-docs  notify readthedocs.io of a (possible) change"
	@echo "            in the public git repository."
	@echo "            readthedocs.io is expected to perform necessary"
	@echo "            re-builds-"
	@echo "  docs      produces all documentation into directory"
	@echo "            'build/docs/'."
	@echo "  clean     remove any files produced by the 'dist' and 'docs'"
	@echo "            targets."
	@echo ""
	@echo "Available targets for development:"
	@echo ""
	@echo "  generate  (re-)generate source code. generated code is comitted"
	@echo "            into git."
#	@echo "  mrproper  same the 'clean' target, plus also removes any files"
#	@echo "            produced by the 'generate' target."

READTHEDOCS_TOKEN := $(shell cat .readthedocs.token)

.PHONY: all
all: clean dist docs

.PHONY: generate
generate: src/enijo/connector/rest_api_client \
          docs/_downloads/invenio-api.json \
          docs/_static/openapi-html \
          docs/_static/openapi-html2 \
          docs/_static/openapi-dynamic-html \
          docs/_static/swagger-ui \
          requirements.txt

.PHONY: dist
dist:
	python3 -m build -x -n

.PHONY: publish-pypi
publish-pypi:
	python3 -m twine upload dist/*

.PHONY: publish-docs
publish-docs:
	curl -X POST -d branches=main -d token=$(READTHEDOCS_TOKEN) \
	  https://readthedocs.org/api/v2/webhook/enijo-connector/162834/

.PHONY: docs
docs: build/docs

.PHONY: clean
clean:
	rm -rf "$(BUILDDIR)"
	rm -rf "$(DISTDIR)"
	rm -rf "$(EXTERNDIR)"

# for example for use by readthedocs.io, export the requirements in the
# 'requirements.txt' file format.
requirements.txt: setup.cfg
	python3 ./setup.py --requires > requirements.txt

INVENIO_API_FILES := $(shell find openapi/invenio-api.d/ -type f)
$(BUILDDIR)/openapi/invenio-api.json: openapi/invenio-api.yaml $(INVENIO_API_FILES)
	@mkdir -p "$(BUILDDIR)/openapi"
	python3 scripts/merge-openapi-json.py \
	    openapi/invenio-api.yaml openapi/invenio-api.d \
	    > "$(BUILDDIR)/openapi/invenio-api~.json"
	pipx run openapi-spec-validator "$(BUILDDIR)/openapi/invenio-api~.json"
	mv "$(BUILDDIR)/openapi/invenio-api~.json" "$@"

src/enijo/connector/rest_api_client: $(BUILDDIR)/openapi/invenio-api.json
	@mkdir -p "$@"
	pipx run openapi-python-client update \
	    --path $(BUILDDIR)/openapi/invenio-api.json \
	    --config openapi/openapi-python-client.conf
	echo "Generated with" > "$@/README"
	pipx run openapi-python-client --version >> "$@/README"
	touch "$@"

$(EXTERNDIR):
	@mkdir $(EXTERNDIR)

$(EXTERNDIR)/openapi-generator-cli.jar: $(EXTERNDIR)
	scripts/download-latest-openapi-cli.py \
	    org.openapitools/openapi-generator-cli \
	    > extern/openapi-generator-cli.jar

$(EXTERNDIR)/swagger-ui-dist: $(EXTERNDIR)
	rm -rf $(BUILDDIR)/extern-swagger-ui-dist
	mkdir -p $(BUILDDIR)/extern-swagger-ui-dist
	wget -q -O- https://codeload.github.com/swagger-api/swagger-ui/tar.gz/refs/tags/v3.51.0 \
	    | tar -xz -C $(BUILDDIR)/extern-swagger-ui-dist
	rm -rf "$@"
	mkdir -p "$@"
	cp -rp -t "$@" $(BUILDDIR)/extern-swagger-ui-dist/swagger-ui-3.51.0/dist/*
	cp -rp -t "$@" $(BUILDDIR)/extern-swagger-ui-dist/swagger-ui-3.51.0/LICENSE

docs/_downloads/invenio-api.json: $(BUILDDIR)/openapi/invenio-api.json
	@mkdir -p docs/_downloads
	cp "$^" "$@"

docs/_static/openapi-html: \
        $(BUILDDIR)/openapi/invenio-api.json \
        $(EXTERNDIR)/openapi-generator-cli.jar
	rm -rf "$@"
	mkdir -p "$@"
	java -jar $(EXTERNDIR)/openapi-generator-cli.jar generate \
	   -g html \
	   -i "$(BUILDDIR)/openapi/invenio-api.json" \
	   -o "$@"

docs/_static/openapi-html2: \
        $(BUILDDIR)/openapi/invenio-api.json \
        $(EXTERNDIR)/openapi-generator-cli.jar
	rm -rf "$@"
	mkdir -p "$@"
	java -jar $(EXTERNDIR)/openapi-generator-cli.jar generate \
	   -g html2 \
	   -i "$(BUILDDIR)/openapi/invenio-api.json" \
	   -o "$@"

docs/_static/openapi-dynamic-html: \
        $(BUILDDIR)/openapi/invenio-api.json \
        $(EXTERNDIR)/openapi-generator-cli.jar
	rm -rf "$@"
	mkdir -p "$@"
	java -jar $(EXTERNDIR)/openapi-generator-cli.jar generate \
	   -g dynamic-html \
	   -i "$(BUILDDIR)/openapi/invenio-api.json" \
	   -o "$@"

docs/_static/swagger-ui: \
        openapi/swagger-ui.html \
        $(EXTERNDIR)/swagger-ui-dist \
        $(BUILDDIR)/openapi/invenio-api.json
	mkdir -p "$@"
	cp -rp -t "$@" $(EXTERNDIR)/swagger-ui-dist/*
	python3 -c 'import sys; \
	    html = open(sys.argv[1], mode="r", encoding="utf-8").read(); \
	    invenio_json = open(sys.argv[2], mode="r", encoding="utf-8").read(); \
	    html = html.replace("%%INVENIO_JSON%%", invenio_json); \
	    sys.stdout.write(html);' \
	    openapi/swagger-ui.html \
	    "$(BUILDDIR)/openapi/invenio-api.json" \
	    > "$@/swagger-ui.html"
	touch "$@"

DOCS_RST_FILES := $(shell find ./docs/ -name "*.rst" -type f)
DOCS_DOWNLOAD_FILES := $(shell find ./docs/_downloads/ -type f)
.PHONY: build/docs
build/docs: \
        docs/conf.py \
        docs/Makefile \
        $(DOCS_RST_FILES) \
        $(DOCS_DOWNLOAD_FILES) \
        docs/_downloads/invenio-api.json \
        docs/_static/openapi-html \
        docs/_static/openapi-html2 \
        docs/_static/openapi-dynamic-html \
        docs/_static/swagger-ui
	$(MAKE) -C docs html
	touch "$@"
