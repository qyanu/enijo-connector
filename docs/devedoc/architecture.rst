.. SPDX-License-Identifier: MIT
.. Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
.. Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
.. This file forms part of the 'enijo-connector' project, see the
   project's readme, notes, and other documentation, including the
   license information, for further details.


Architecture
============

At the top level, the single ``EnijoConnector`` class provides methods
on a per-use-case basis. That is, every significant use-case corresponds
to roughly a single method invocation of a configured EnijoConnector
instance.

At the "direct interface" level, the various ``*Connector`` classes
offer an API-centric view of the same functionality. One api call over
http transport corresponds to one method invocation. Connector instances
are conveniently created and returned, so the data repository can be
"explored" through the code.

At the low level, the auto-generated ``rest_api_client`` is a direct
result of the OpenApi description of the api. Due to tooling features
(or lack there of), this auto-generated client might actually exhibit
quirky behaviour that is fixed when using methods from the ``*Connector``
classes of the direct interface (and thus also when using the top-level
``EnijoConnector``).
