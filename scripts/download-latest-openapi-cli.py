#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
# Copyright 2021 Max-Julian Pogner <max-julian@pogner.at>
# Copyright 2021 Tobias Hajszan <tobias.hajszan@outlook.com>
#
# This file forms part of the 'enijo-connector' project, see the
# project's readme, notes, and other documentation, including the
# license information, for further details.

"""
This command line script downloads the latest version of a maven
package from maven central.
"""

import argparse
import logging
import re
import requests
import sys
from bs4 import BeautifulSoup


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("PACKAGE", help="the package name, for example "
    "'openapitools/openapi-generator-cli'")
ARGS = parser.parse_args()


BASEURL = "https://repo1.maven.org/maven2"


class MavenVersion:
    """
    parses a version string according to
    http://maven.apache.org/pom.html#Version_Order_Specification
    and provide comparison operators.
    """
    def __init__(self, verstr):
        self._ver = self._split(verstr)

    def __repr__(self):
        return self.version()

    def __lt__(self, other):
        if type(self) != type(other):
            return NotImplemented
        s_len = len(self._ver)
        o_len = len(other._ver)
        minlen = min(s_len, o_len)
        for i in range(minlen):
            s_dot = '.' == self._ver[i][0]
            s_dig = self._ver[i][1].isdigit()
            o_dot = '.' == other._ver[i][0]
            o_dig = other._ver[i][1].isdigit()
            num_map = {
                (True, False): 1,
                (False, False): 2,
                (False, True): 3,
                (True, True): 4
            }
            s_num = num_map.get((s_dot, s_dig))
            o_num = num_map.get((o_dot, o_dig))
            if s_num != o_num:
                return s_num < o_num

            if s_dig and o_dig:
                s_num = int(self._ver[i][1:])
                o_num = int(other._ver[i][1:])
                if s_num == o_num:
                    continue
                else:
                    return s_num < o_num

            qual_map = {
                "alpha": 1,
                "beta": 2,
                "milestone": 3,
                "rc": 4,
                "cr": 4,
                "snapshot": 5,
                "": 6,
                "final": 6,
                "ga": 6,
                "sp": 7,
            }
            s_qual = self._ver[i][1:]
            o_qual = other._ver[i][1:]
            s_special = s_qual.lower() in qual_map
            o_special = o_qual.lower() in qual_map
            if not s_special and o_special:
                return True
            if s_special and not o_special:
                return False
            if not s_special:
                return s_qual < o_qual
            s_num = qual_map[s_qual.lower()]
            o_num = qual_map[o_qual.lower()]
            if s_num != o_num:
                return s_num < o_num
        if s_len != o_len:
            return s_len < o_len
        return False

    def __le__(self, other):
        return NotImplemented

    def __eq__(self, other):
        if type(self) != type(other):
            return NotImplemented
        lt = self < other
        gt = other < self
        if not lt and not gt:
            return True
        return False
    def __ne__(self, other):
        return not self.__eq__(other)
    def __gt__(self, other):
        return NotImplemented
    def __ge__(self, other):
        return NotImplemented

    def version(self):
        """returns self as a normal version string"""
        return ''.join(self._ver)[1:]

    def _split(self, verstr):
        """split and return tuple"""
        ver = [['.']]
        last_c = '.'
        for i in range(len(verstr)):
            c = verstr[i]
            if '.' == c:
                ver.append([c])
            elif '-' == c:
                ver.append([c])
            elif c.isdigit() and not last_c.isdigit():
                if '.' != last_c and '-' != last_c:
                    ver.append(['-'])
                ver[-1].append(c)
            elif not c.isdigit() and last_c.isdigit():
                if '.' != last_c and '-' != last_c:
                    ver.append(['-'])
                ver[-1].append(c)
            else:
                ver[-1].append(c)
            last_c = c
        collapsed = []
        for e in ver:
            e = ''.join(e)
            if '-' == e:
                e = '-0'
            elif '.' == e:
                e = '.0'
            collapsed.append(e)
        ver = []
        trailing = True
        for e in reversed(collapsed):
            if e not in (".0", "-0", ".", "-", ".final", "-final", ".ga", "-ga"):
                trailing = False
            if not trailing:
                ver.append(e)
            if '-' == e[0]:
                trailing = True
        return tuple(reversed(ver))


url = f"{BASEURL}/{ARGS.PACKAGE.replace('.','/')}/"
logger.info("url: %s", url)
r = requests.get(url)
soup = BeautifulSoup(r.text, 'html.parser')

maximum_version = None

# maven has exactly defined version semantics not currently considered here
rex_version_href = re.compile('^(?P<version>[^.].*)/$')
for entry in soup.find_all('a'):
    m = rex_version_href.match(entry.get('href'))
    if not m:
        continue
    entry_version = MavenVersion(m.group("version"))
    if None == maximum_version or maximum_version < entry_version:
        maximum_version = entry_version

maximum_version = maximum_version.version()

artifact_id = ARGS.PACKAGE.split("/")[1]

url = f"{BASEURL}/{ARGS.PACKAGE.replace('.','/')}/{maximum_version}/{artifact_id}-{maximum_version}.jar"
logger.info("url: %s", url)
r = requests.get(url, stream=True)
sys.stdout.buffer.write(r.content)
